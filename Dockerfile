ARG BASE_IMG

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-ci"]
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /workspace/ros_ws

RUN apt update && apt install -y \
    libspdlog-dev iputils-ping wireshark tshark iproute2 libsdl-dev libgtk2.0-dev  \ 
    ros-noetic-camera-info-manager ros-noetic-roslint \
    ros-noetic-teleop-twist-keyboard && \
    rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install opencv-contrib-python
RUN python3 -m pip install git+https://gitlab.com/beerlab/cpc/quadcopter/yolo_water_wrapper.git
COPY ./ /workspace/ros_ws/src/coast_following

RUN catkin_make
